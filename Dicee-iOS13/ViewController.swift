//
//  ViewController.swift
//  Dicee-iOS13
//
//  Created by Angela Yu on 11/06/2019.
//  Copyright © 2019 London App Brewery. All rights reserved.
//

import UIKit
import CLTypingLabel

class ViewController: UIViewController {

    @IBOutlet weak var diceImageOne: UIImageView!
    
    @IBOutlet weak var diceImageTwo: UIImageView!
    
    @IBOutlet weak var diceImageThree: UIImageView!
    @IBOutlet weak var labelTitle: CLTypingLabel!
    
    var diceImageArray = [#imageLiteral(resourceName: "DiceOne"),#imageLiteral(resourceName: "DiceTwo"),#imageLiteral(resourceName: "DiceThree")]
    
    var randomDice1 = 0
    var randomDice2 = 0
    var randomDice3 = 0
    
    override func viewDidLoad() {
        super .viewDidLoad()
        labelTitle.text = " "
    }
    
    @IBAction func rollButoon(_ sender: UIButton) {
        randomDice1 = Int.random(in: 0...2)
        randomDice2 = Int.random(in: 0...2)
        randomDice3 = Int.random(in: 0...2)

        diceImageOne.image = diceImageArray[randomDice1]
        diceImageTwo.image = diceImageArray[randomDice2]
        diceImageThree.image = diceImageArray[randomDice3]
        
        if randomDice1 == randomDice2 && randomDice1 == randomDice3 {
            print("you WIN")
            labelTitle.text = "You Win"
        } else {
            labelTitle.text = ""
        }

        
    }
    
    
    
}

